# Python app deployment on Kubernetes

## App Improvements:
- python-dotenv added
- Dockerfile created 

## Kubernetes Manifest:
- Deployment
- Service (ClusterIP)
- Configmap (just for FLASK_APP var)
- Secret (for APP_AES_KEY var - value updated in Gitlab pipeline by sec command)
- Horizontal Pod Autoscaler (CPU and Memory average utilization: 70% - EKS Cluster must have metrics-server enabled)

## Gitlab pipeline
- Build: Utilizes a Docker image to build and push app image to Dockerhub (https://hub.docker.com/repository/docker/evandromorini/tiqets-app)
- Deploy: Deploy app on a EKS Cluster, installing and configuring kubectl access, updating secret from manifest and applying YAML file
