FROM python:3.6.13

WORKDIR /usr/src/app

COPY requirements.txt ./
RUN apt-get update && apt-get install -y \
  python3-dev \
  build-essential 
RUN pip install --no-cache-dir -r requirements.txt


COPY . .

CMD [ "python", "app.py" ]

EXPOSE 5000
